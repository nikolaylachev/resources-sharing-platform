Resources Sharing Platform
==========================

This project is the main part of my diploma thesis for my bachelor degree at the University Of National And World Economy.


# Project goals:

- Creating a platform through which users can store important data in the form of resources;
- The platform should give full freedom with regard to the content of the data that the users want to save but it has to be in the JSON format;
- Moreover the platform should take care of things like where, how, and on what servers to save the users' data and just give them easy access to it;
- Creating an API that users can use to easily retrieve, add, edit, and remove data from their own resources or the ones shared with them. You can check out more about the API here;
- Finally the platform should give the users the ability to share their own resources with other users of the platform.
