<?php

namespace AppBundle\Controller;

use AppBundle\Entity\PaymentPlan;
use AppBundle\Entity\User;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegisterController extends Controller
{
    /**
     * @Route("/register")
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return RedirectResponse|Response
     */
    public function indexAction(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $user = new User();

        $form = $this->createFormBuilder($user)
            ->add('username', TextType::class, ['label' => 'Username * '])
            ->add('first_name', TextType::class, ['required' => false , 'label' => 'First name '])
            ->add('last_name', TextType::class, ['required' => false, 'label' => 'Last name '])
            ->add('password', RepeatedType::class, [
                'type' => PasswordType::class,
                'first_options'  => ['label' => 'Password * '],
                'second_options' => ['label' => 'Repeat Password * '],
                'invalid_message' => 'Password and repeat password do not match.'
            ])
            ->add('register', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        $validator = $this->get('validator');
        $errors = $validator->validate($user);

        if (count($errors) > 0) {
            $errorsForView = array();

            foreach ($errors as $error) {
                $errorsForView[] = $error->getMessage();
            }

            unset($errors);

            return $this->render('register/register.html.twig', [
                'form' => $form->createView(),
                'errors' => $errorsForView
            ]);
        }

        if ($request->get('form')['password']['first'] !== $request->get('form')['password']['second']) {
            return $this->render('register/register.html.twig', [
                'form' => $form->createView(),
                'errors' => ['Password and repeat password are not the same.']
            ]);
        }

        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();

            if (!is_null($entityManager
                        ->getRepository(User::class)
                        ->findOneBy(['username' => $user->getUsername()])
            )){
                return $this->render('register/register.html.twig', [
                    'form' => $form->createView(),
                    'errors' => ['This username already exists']
                ]);
            }

            $password = $passwordEncoder->encodePassword($user, $user->getPassword());
            $user->setPassword($password);

            try {

                $paymentPlan = $entityManager
                                ->getRepository(PaymentPlan::class)
                                ->find(1);

                $user->setPaymentPlan($paymentPlan);

                $entityManager->persist($user);
                $entityManager->flush();

                return $this->redirectToRoute('login');
            } catch (\Exception $exception){
                return $this->render('register/register.html.twig', [
                    'form' => $form->createView(),
                    'errors' => [$exception->getMessage()]
                ]);
            }
        }

        return $this->render('register/register.html.twig', ['form' => $form->createView()]);
    }
}
