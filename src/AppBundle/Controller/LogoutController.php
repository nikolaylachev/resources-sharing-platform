<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class LogoutController extends Controller
{
    /**
     * @Route("/logout", name="logout")
     * @param Request $request
     * @param SessionInterface $session
     * @return Response
     */
    public function indexAction(Request $request, SessionInterface $session)
    {

        $session->remove('user_id');
        $session->remove('fullName');
        $session->remove('username');
        return $this->redirectToRoute('homepage');
    }
}
