<?php

namespace AppBundle\Controller;

use AppBundle\Entity\PaymentPlan;
use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class PaymentPlanController extends Controller
{
    /**
     * @Route("/plan/buy/{id}", requirements={"id"="\d+"}, name="buyPaymentPlan")
     * @param $id
     * @param SessionInterface $session
     * @return Response
     */
    public function buyAction($id, SessionInterface $session)
    {
        if (is_null($session->get('user_id'))){
            return $this->redirectToRoute('login');
        }

        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->find($session->get('user_id'));


        $paymentPlan = $this->getDoctrine()
                        ->getRepository(PaymentPlan::class)
                        ->find($id);


        $user->setPaymentPlan($paymentPlan);

        $this->getDoctrine()
            ->getManager()
            ->flush();

        return $this->redirectToRoute('allResources');

    }
}
