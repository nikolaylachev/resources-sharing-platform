<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class LoginController extends Controller
{
    /**
     * @Route("/login", name="login")
     * @param Request $request
     * @param SessionInterface $session
     * @param UserPasswordEncoderInterface $encoder
     * @return Response
     */

    public function indexAction(Request $request, SessionInterface $session, UserPasswordEncoderInterface $encoder)
    {

        if (!is_null($session->get('user_id'))){
            return $this->redirectToRoute('allResources');
        }

        $user = new User();

        $form = $this->createFormBuilder($user)
            ->add('username', TextType::class, ['label' => 'Username'])
            ->add('password', PasswordType::class, ['label' => 'Password'])
            ->add('login', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        $validator = $this->get('validator');
        $errors = $validator->validate($user);


        if (count($errors) > 0) {
            $errorsForView = array();

            foreach ($errors as $error) {
                $errorsForView[] = $error->getMessage();
            }

            unset($errors);

            return $this->render('login/login.html.twig', [
                'form' => $form->createView(),
                'errors' => $errorsForView
            ]);
        }

        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$user` variable has also been updated
            $user = $form->getData();

            $plainPassword = $user->getPassword();

            $repository = $this->GetDoctrine()
                ->getManager()
                ->getRepository(User::class);

            $user = $repository->findOneBy([
               'username' => $user->getUsername()
            ]);

            if(!is_null($user)){

                if ($encoder->isPasswordValid($user, $plainPassword)){
                    $session->set('user_id', $user->getId());

                    if(is_null($user->getFirstName()) or is_null($user->getLastName())){
                        $session->set('username', $user->getUsername());
                    } else {
                        $session->set('fullName', ($user->getFirstName() . ' ' . $user->getLastName()));
                    }

                    return $this->redirectToRoute('allResources');
                }

                return $this->render('login/login.html.twig', [
                    'form' => $form->createView(),
                    'errors' => ['Incorrect username or password.']
                ]);
            }

            return $this->render('login/login.html.twig', [
                'form' => $form->createView(),
                'errors' => ['Incorrect username or password.']
            ]);
        }

        return $this->render('login/login.html.twig',  ['form' => $form->createView()]);
    }
}
