<?php

namespace AppBundle\Controller;

use AppBundle\Entity\AccessLevel;
use AppBundle\Entity\User;
use AppBundle\Entity\UsersResources;
use AppBundle\Service\Mongodb\MongodbServiceInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Security;
use \MongoClient;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
//        return $this->render('default/index.html.twig', [
//            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
//        ]);
        return $this->redirectToRoute('login');
    }

    /**
     * @Route("/test", name="test")
     * @param Request $request
     * @param SessionInterface $session
     * @param UserPasswordEncoderInterface $encoder
     * @param MongodbServiceInterface $mongodbService
     * @return Response
     */
    public function testAction(Request $request, SessionInterface $session,UserPasswordEncoderInterface $encoder, MongodbServiceInterface $mongodbService)
    {
//        $session->remove('user_id');
//        $entityManager = $this->GetDoctrine()->getManager();
//        $repository = $entityManager->getRepository(UsersResources::class);
//        $userR = new UsersResources();

//        $userResource = $repository->findOneBy(['user_id' => 1, 'resource_id' => 4, 'access_level_id' => 4]);
//
//        var_dump($userResource->getApiKey());
//        exit;

        $document = $mongodbService->getDocument('5cc75a1986201e2281028d9d');

//        var_dump(
//            (MongoDB\BSON\toJSON(
//                MongoDB\BSON\fromPHP($document)
//            ))
//        );exit;
//        $strDocument = $mongodbService->getJsonDocument('5cc75a1986201e2281028d9d');

        return $this->render('test.html.twig', ['test' => ($document)]);

    }
}
