<?php

namespace AppBundle\Controller;

use AppBundle\Entity\AccessLevel;
use AppBundle\Entity\PaymentPlan;
use AppBundle\Entity\Resource;
use AppBundle\Entity\User;
use AppBundle\Entity\UsersResources;
use AppBundle\Service\Mongodb\MongodbServiceInterface;
use AppBundle\Service\Resources\ResourcesServiceInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class ResourcesController extends Controller
{
    private $mongodbService;

    public function __construct(MongodbServiceInterface $mongodbService)
    {
        $this->mongodbService = $mongodbService;
    }

    /**
     * @Route("/resource/{id}", requirements={"id"="\d+"}, name="viewResource")
     * @param $id
     * @param SessionInterface $session
     * @param Request $request
     * @return Response
     */
    public function viewAction($id, SessionInterface $session, Request $request)
    {

        if (is_null($session->get('user_id'))){
            return $this->redirectToRoute('login');
        }

        if (empty(
                    $this->getDoctrine()
                    ->getRepository(Resource::class)
                    ->getResourceByUserIdAndResourceId($session->get('user_id'), $id)
        )) {
            return new Response('<p>This user cannot see this resource. </p>');
        }

        $mongoId = $this->getDoctrine()
                    ->getRepository(Resource::class)
                    ->getResourceMongoIdByResourceId($id);

        $document = $this->mongodbService->getDocument($mongoId);

        $accessLevel = $this->getDoctrine()
            ->getRepository(Resource::class)
            ->getResourceAccessLevelIdAndNameByUserIdAndResourceId($session->get('user_id'), $id);

        $resourceDetails = $this->getDoctrine()
            ->getRepository(Resource::class)
            ->getResourceDetails($session->get('user_id'), $id);

        $apiUri = str_replace('8004', '8002', $request->server->get('HTTP_HOST'));

        return $this->render('resource/byId.html.twig',
            [
                'document' => $document,
                'accessLevel' => $accessLevel[0]['name'],
                'id' => $id,
                'resourceDetails' => $resourceDetails[0],
                'apiUri' => $apiUri
            ]
        );
    }

    /**
     * @Route("/resource/create", name="createResource")
     * @param Request $request
     * @param SessionInterface $session
     * @return Response
     */
    public function createAction(Request $request, SessionInterface $session)
    {
        if (is_null($session->get('user_id'))){
            return $this->redirectToRoute('login');
        }

        $numberOfResourcesOfUser = intval(
                            json_decode(
                                $this->checkNumberOfResourcesAction($session)->getContent()
                            )[0]->numberOfResources
        );

        $numberOfResourcesOfPaymentPlan = $this->getDoctrine()
            ->getRepository(PaymentPlan::class)
            ->getNumberOfResourcesForUsersPaymentPlanByUserId($session->get('user_id'))[0]['numberOfResources'];


        if ($numberOfResourcesOfPaymentPlan !== 'Unlimited'){

            $numberOfResourcesOfPaymentPlan = intval($numberOfResourcesOfPaymentPlan);

            if ($numberOfResourcesOfUser >= $numberOfResourcesOfPaymentPlan){
                if ($numberOfResourcesOfPaymentPlan === 3){
                    $paymentPlans = $this->getDoctrine()
                        ->getRepository(PaymentPlan::class)
                        ->getAllPaidPaymentPlans();

                    return $this->render('resource/create.html.twig', [
                       'paymentPlans' => $paymentPlans
                    ]);
                } else if ($numberOfResourcesOfPaymentPlan === 10) {
                    $paymentPlans = $this->getDoctrine()
                        ->getRepository(PaymentPlan::class)
                        ->getLastTwoPaidPaymentPlans();

                    return $this->render('resource/create.html.twig', [
                        'paymentPlans' => $paymentPlans
                    ]);

                } else if ($numberOfResourcesOfPaymentPlan === 100){
                    $paymentPlans = $this->getDoctrine()
                        ->getRepository(PaymentPlan::class)
                        ->getLastPaidPaymentPlan();

                    return $this->render('resource/create.html.twig', [
                        'paymentPlans' => $paymentPlans
                    ]);
                }
            }
        }

        $resource = new Resource();

        $form = $this->createFormBuilder($resource)
            ->add('resourceName', TextType::class, ['label' => 'Resource name '])
            ->add('create', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        $validator = $this->get('validator');
        $errors = $validator->validate($resource);

        if (count($errors) > 0) {
            $errorsForView = array();

            foreach ($errors as $error) {
                $errorsForView[] = $error->getMessage();
            }

            unset($errors);

            return $this->render('resource/create.html.twig', [
                'form' => $form->createView(),
                'errors' => $errorsForView
            ]);
        }

        if ($form->isSubmitted() && $form->isValid()){

            $entityManager = $this->GetDoctrine()->getManager();

            if (!empty($entityManager
                ->getRepository(Resource::class)
                ->checkIfResourceNameExistsByResourceNameAndUserId($resource->getResourceName(), $session->get('user_id'))
            )){
                return $this->render('resource/create.html.twig', [
                    'form' => $form->createView(),
                    'errors' => ['This user has a resource with this name']
                ]);
            }

            $repository = $entityManager->getRepository(User::class);

            $user = $repository->findOneBy(['id' => $session->get('user_id')]);

            $docId = $this->mongodbService->insertDocument();
            $resource->setMongodbId($docId);
            $entityManager->persist($resource);
            $entityManager->flush();


            $usersResources = new UsersResources();
            $usersResources->setUser($user);
            $usersResources->setResource($resource);
            $usersResources->setApiKey(
                base64_encode($user->getUsername() . $resource->getId())
            );

            $repository = $entityManager->getRepository(AccessLevel::class);
            $accessLevel = $repository->find(4);

            $usersResources->setAccessLevel($accessLevel);
            $usersResources->setIsCreator(true);


            $entityManager->persist($usersResources);
            $entityManager->flush();

            //redirect to All resources;
            return $this->redirectToRoute('allResources');
        }


        return $this->render('resource/create.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/resource/all", name="allResources")
     * @param SessionInterface $session
     * @return RedirectResponse|Response
     */
    public function getAllAction(SessionInterface $session)
    {
        if (is_null($session->get('user_id'))){
            return $this->redirectToRoute('login');
        }

        $resources = $this->getDoctrine()
            ->getRepository(Resource::class)
            ->getAllResourcesByUserId($session->get('user_id'));


        $sharedResources = $this->getDoctrine()
            ->getRepository(Resource::class)
            ->getAllSharedResourcesByUserId($session->get('user_id'));


        $numberOfResourcesFromPaymentPlan = $this->getDoctrine()
            ->getRepository(PaymentPlan::class)
            ->getNumberOfResourcesForUsersPaymentPlanByUserId($session->get('user_id'))[0]['numberOfResources'];

        return $this->render('resource/all.html.twig', [
            'resources' => $resources,
            'sharedResources' => $sharedResources,
            'numberOfResourcesFromPaymentPlan' => $numberOfResourcesFromPaymentPlan
        ]);

    }

    /**
     * @Route("resource/share/{id}", requirements={"id"="\d+"}, name="resourceShare")
     * @param $id
     * @param SessionInterface $session
     * @param Request $request
     * @return Response
     */
    public function shareAction($id, SessionInterface $session, Request $request) {
        if (is_null($session->get('user_id'))){
            return $this->redirectToRoute('login');
        }

        if (empty(
                    $this->getDoctrine()
                    ->getRepository(Resource::class)
                    ->checkIfUserIsCreatorOfResourceByUserIdAndResourceId($session->get('user_id'),$id)
        )) {
            return $this->render('resource/share.html.twig', ['message' => 'Only the creator of this resource can share it.']);
        }

        $users = $this->getDoctrine()
            ->getRepository(User::class)
            ->getAllUsersForResourceSharing($id);


        if (count($users) === 0) {
            return $this->render('resource/share.html.twig', ['message' => 'You have already shared this resource with everybody!']);
        }

        $usersForForm = array('' => '');

        foreach ($users as $user){
            $usersForForm[$user['username']] = $user['id'];
        }

        $accessLevels = $this->getDoctrine()
                        ->getRepository(AccessLevel::class)
                        ->getAllAccessLevels();


        $accessLevelsForForm = array('' => '');

        foreach ($accessLevels as $accessLevel){
            $accessLevelsForForm[$accessLevel['name']] = $accessLevel['id'];
        }

        unset($users, $accessLevels);

        $resourceName = $this->getDoctrine()
                        ->getRepository(Resource::class)
                        ->getResourceNameById($id)[0]['resourceName'];

        $form = $this->createFormBuilder()
            ->add('user', ChoiceType::class,[
                'label' => 'Users ',
                'choices' => $usersForForm
            ])
            ->add('accessLevel', ChoiceType::class,[
                'label' => 'Access Levels ',
                'choices' => $accessLevelsForForm
            ])
            ->add('share', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $entityManager = $this->GetDoctrine()->getManager();


            $user = $this->getDoctrine()
                    ->getRepository(User::class)
                    ->findOneBy(['id' => $form->getData()['user']]);

            $accessLevel = $this->getDoctrine()
                            ->getRepository(AccessLevel::class)
                            ->findOneBy(['id' => $form->getData()['accessLevel']]);

            $resource = $this->getDoctrine()
                        ->getRepository(Resource::class)
                        ->find($id);


            $usersResources = new UsersResources();

            $usersResources->setUser($user);
            $usersResources->setResource($resource);
            $usersResources->setAccessLevel($accessLevel);
            $usersResources->setIsCreator(false);
            $usersResources->setApiKey(base64_encode($user->getUsername() . $resource->getId()));

            try{
                $entityManager->persist($usersResources);
                $entityManager->flush();

                return $this->redirectToRoute('allResources');
            }catch (\Exception $exception){
                return $this->render('resource/share.html.twig', [
                    'form' => $form->createView(),
                    'message' => $exception->getMessage(),
                    'resourceName' => $resourceName
                ]);
            }

        }

        return $this->render('resource/share.html.twig', ['form' => $form->createView(), 'resourceName' => $resourceName]);

    }

    /**
     * @param $id
     * @param SessionInterface $session
     * @return RedirectResponse
     * @Route("resource/delete/{id}", requirements={"id"="\d+"}, name="deleteResource")
     */
    public function deleteAction($id, SessionInterface $session) {
        if (is_null($session->get('user_id'))){
            return $this->redirectToRoute('login');
        }

        if (empty(
            $this->getDoctrine()
                ->getRepository(Resource::class)
                ->checkIfUserIsCreatorOfResourceByUserIdAndResourceId($session->get('user_id'),$id)
            )) {
            return $this->redirectToRoute('allResources');
        }

        $mongoId = $this->getDoctrine()
            ->getRepository(Resource::class)
            ->getResourceMongoIdByResourceId($id);

        $this->getDoctrine()
            ->getRepository(UsersResources::class)
            ->deleteResourceByResourceId($id);

        $this->getDoctrine()
            ->getRepository(Resource::class)
            ->deleteResourceByResourceId($id);


        $this->mongodbService->deleteDocument($mongoId);

        return $this->redirectToRoute('allResources');

    }

    /**
     * @param $id
     * @param Request $request
     * @param SessionInterface $session
     * @return Response
     * @Route("resource/changeResource/{id}", requirements={"id"="\d+"}, name="resourceChange")
     */
    public function changeResourceAction($id, Request $request, SessionInterface $session) {
        if (is_null($session->get('user_id'))){
            return $this->redirectToRoute('login');
        }

        if (empty(
        $this->getDoctrine()
            ->getRepository(Resource::class)
            ->checkIfUserIsCreatorOfResourceByUserIdAndResourceId($session->get('user_id'),$id)
        )) {
            return $this->redirectToRoute('viewResource', $id);
        }

        $data = json_decode($request->getContent(),true);
        $mongodbId = $data['_id']['$oid'];
        unset($data['_id']);

        try {
            $this->mongodbService->updateDocument($mongodbId, $data);
            return new Response('Update successful!');
        }catch (\Exception $exception){
            return new Response('Something went wrong!');
        }
    }


    /**
     * @param Request $request
     * @param SessionInterface $session
     * @Route("resource/search", name="searchForResources")
     * @return Response
     */
    public function searchAction(Request $request, SessionInterface $session){
        if (is_null($session->get('user_id'))){
            return $this->redirectToRoute('login');
        }

        $searchResult = $this->getDoctrine()
                        ->getRepository(Resource::class)
                        ->searchForResources(
                            json_decode($request->getContent(), true),
                            $session->get('user_id')
                        );

        return $this->json($searchResult);
    }

    /**
     * @Route("resource/checkNumberOfResources", name="checkNumberOfResources")
     * @param SessionInterface $session
     * @return JsonResponse
     */
    public function checkNumberOfResourcesAction(SessionInterface $session){
        $numberOfResources = $this->getDoctrine()
                            ->getRepository(Resource::class)
                            ->getNumberOfResourcesOfUserByUserId($session->get('user_id'));

        return $this->json($numberOfResources);
    }
}
