<?php


namespace AppBundle\Service\Resources;


use AppBundle\Entity\Resource;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class ResourcesService extends Controller implements ResourcesServiceInterface
{

    public function viewResource($id, SessionInterface $session)
    {
        if (is_null($session->get('user_id'))){
            return $this->redirectToRoute('login');
        }

//        var_dump($this->getDoctrine());exit;

        if (empty(
        $this->getDoctrine()
            ->getRepository(Resource::class)
            ->getResourceByUserIdAndResourceId($session->get('user_id'), $id)
        )) {
            return new Response('<p>This user cannot see this resource. </p>');
        }

        $mongoId = $this->getDoctrine()
            ->getRepository(Resource::class)
            ->getResourceMongoIdByResourceId($id);

        $document = $this->mongodbService->getDocument($mongoId);

        $accessLevel = $this->getDoctrine()
            ->getRepository(Resource::class)
            ->getResourceAccessLevelIdAndNameByUserIdAndResourceId($session->get('user_id'), $id);

        $resourceDetails = $this->getDoctrine()
            ->getRepository(Resource::class)
            ->getResourceDetails($session->get('user_id'), $id);

        return $this->render('resource/byId.html.twig',
            [
                'document' => $document,
                'accessLevel' => $accessLevel[0]['name'],
                'id' => $id,
                'resourceDetails' => $resourceDetails[0]
            ]
        );
    }
}