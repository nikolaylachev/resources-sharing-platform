<?php


namespace AppBundle\Service\Resources;

use Symfony\Component\HttpFoundation\Session\SessionInterface;

interface ResourcesServiceInterface
{
    public function viewResource($id, SessionInterface $session);
}