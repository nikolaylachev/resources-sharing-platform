<?php


namespace AppBundle\Service\Mongodb;


use AppBundle\Service\Mongodb\MongodbServiceInterface;

class MongodbService implements MongodbServiceInterface
{
    private $connection;

    private $collection;

    const PORT_NUMBER = '27017';

    public function __construct()
    {
        $this->connection = new \MongoDB\Client('mongodb://localhost:' . self::PORT_NUMBER);
        $this->collection = $this->connection->php_api->resources;
    }


    public function insertDocument($document = array())
    {
        $insertOneResult = $this->collection->insertOne($document);
        return (string) $insertOneResult->getInsertedId();
    }

    public function getDocument($mongoId)
    {
//        var_dump($this->collection->findOne(['_id' => $mongoId]));exit;
        return $this->collection->findOne([
            '_id' =>  new \MongoDB\BSON\ObjectId($mongoId)
        ]);
    }

    public function getJsonDocument($mongoId){
        //return new MongoDB\BSON\toJSON(MongoDB\BSON\fromPHP($this->getDocument($mongoId)));
    }

    public function updateDocument($mongodbId, $data)
    {
        return $this->collection->replaceOne(
                [ '_id' => new \MongoDB\BSON\ObjectId($mongodbId) ],
                $data
        );
    }

    public function deleteDocument($mongodbId)
    {
        return $this->collection->deleteOne(
            ['_id' => new \MongoDB\BSON\ObjectId($mongodbId)]
        );
    }
}