<?php


namespace AppBundle\Service\Mongodb;


interface MongodbServiceInterface
{
    public function insertDocument($document = null);
    public function getDocument($mongoId);
    public function updateDocument($mongodbId, $data);
    public function deleteDocument($mongodbId);
}