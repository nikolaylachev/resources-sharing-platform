<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use  \Doctrine\Common\Collections\ArrayCollection as ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Resource
 *
 * @ORM\Table(name="resources")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ResourceRepository")
 */
class Resource
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="mongodb_id", type="string", length=150, unique=true)
     */
    private $mongodbId;

    /**
     * @var string
     * @Assert\Length(
     *     min="3",
     *     max="255",
     *     minMessage = "Your resource name must be at least {{ limit }} characters long",
     *     maxMessage = "Your resource name cannot be longer than {{ limit }} characters"
     * )
     * @Assert\Regex(
     *     pattern="/^[\w\ \.]+$/",
     *     match=true,
     *     message="Your resource name must contain only letters, digits and spaces."
     * )
     * @ORM\Column(name="resource_name", type="string", length=255)
     */
    private $resourceName;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;

    /**
     * One Resource have Many UsersResources
     * @ORM\OneToMany(targetEntity="UsersResources", mappedBy="resource")
     */
    private $users_resources;


    /**
     * Resource constructor.
     */
    public function __construct()
    {
        $this->users_resources = new ArrayCollection();
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set mongodbId
     *
     * @param string $mongodbId
     *
     * @return Resource
     */
    public function setMongodbId($mongodbId)
    {
        $this->mongodbId = $mongodbId;

        return $this;
    }

    /**
     * Get mongodbId
     *
     * @return string
     */
    public function getMongodbId()
    {
        return $this->mongodbId;
    }

    /**
     * Set resourceName
     *
     * @param string $resourceName
     *
     * @return Resource
     */
    public function setResourceName($resourceName)
    {
        $this->resourceName = $resourceName;

        return $this;
    }

    /**
     * Get resourceName
     *
     * @return string
     */
    public function getResourceName()
    {
        return $this->resourceName;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Resource
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Resource
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @return mixed
     */
    public function getUsersResources()
    {
        return $this->users_resources;
    }

}

