<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * PaymentPlan
 *
 * @ORM\Table(name="payment_plan")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PaymentPlanRepository")
 */
class PaymentPlan
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="numberOfResources", type="string", length=20)
     */
    private $numberOfResources;

    /**
     * @var string
     *
     * @ORM\Column(name="price", type="decimal", precision=10, scale=2)
     */
    private $price;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\User", mappedBy="paymentPlan")
     */
    private $users;

    /**
     * PaymentPlan constructor.
     */
    public function __construct()
    {
        $this->users = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return PaymentPlan
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set numberOfResources
     *
     * @param string $numberOfResources
     *
     * @return PaymentPlan
     */
    public function setNumberOfResources($numberOfResources)
    {
        $this->numberOfResources = $numberOfResources;

        return $this;
    }

    /**
     * Get numberOfResources
     *
     * @return string
     */
    public function getNumberOfResources()
    {
        return $this->numberOfResources;
    }

    /**
     * Set price
     *
     * @param string $price
     *
     * @return PaymentPlan
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }
}

