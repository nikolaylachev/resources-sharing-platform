<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use  \Doctrine\Common\Collections\ArrayCollection as ArrayCollection;

/**
 * AccessLevel
 *
 * @ORM\Table(name="access_level")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AccessLevelRepository")
 */
class AccessLevel
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * One Resource have Many UsersResources
     * @ORM\OneToMany(targetEntity="UsersResources", mappedBy="access_level")
     */
    private $users_resources;

    /**
     * AccessLevel constructor.
     */
    public function __construct()
    {
        $this->users_resources = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return AccessLevel
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getUsersResources()
    {
        return $this->users_resources;
    }
}

