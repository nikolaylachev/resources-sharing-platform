<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UsersResources
 *
 * @ORM\Table(name="users_resources")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UsersResourcesRepository")
 */
class UsersResources
{
//    /**
//     * @var int
//     *
//     * @ORM\Column(name="id", type="integer")
//     * @ORM\Id
//     * @ORM\GeneratedValue(strategy="AUTO")
//     */
//    private $id;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="User", inversedBy="users_resources")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Resource", inversedBy="users_resources")
     * @ORM\JoinColumn(name="resource_id", referencedColumnName="id")
     */
    private $resource;

    /**
     * @ORM\ManyToOne(targetEntity="AccessLevel", inversedBy="users_resources")
     * @ORM\JoinColumn(name="access_level_id", referencedColumnName="id")
     */
    private $access_level;

    /**
     * @var string
     *
     * @ORM\Column(name="api_key", type="string", length=255, unique=true)
     */
    private $apiKey;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_creator", type="boolean")
     */
    private $isCreator;


//    /**
//     * Get id
//     *
//     * @return int
//     */
//    public function getId()
//    {
//        return $this->id;
//    }
    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @param mixed $resource
     */
    public function setResource($resource)
    {
        $this->resource = $resource;
    }

    /**
     * @param mixed $access_level
     */
    public function setAccessLevel($access_level)
    {
        $this->access_level = $access_level;
    }

    /**
     * Set apiKey
     *
     * @param string $apiKey
     *
     * @return UsersResources
     */
    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;

        return $this;
    }

    /**
     * Get apiKey
     *
     * @return string
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }

    /**
     * Set isCreator
     *
     * @param boolean $isCreator
     *
     * @return UsersResources
     */
    public function setIsCreator($isCreator)
    {
        $this->isCreator = $isCreator;

        return $this;
    }

    /**
     * Get isCreator
     *
     * @return bool
     */
    public function getIsCreator()
    {
        return $this->isCreator;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return mixed
     */
    public function getResource()
    {
        return $this->resource;
    }

    /**
     * @return mixed
     */
    public function getAccessLevel()
    {
        return $this->access_level;
    }
}

