<?php

namespace AppBundle\Repository;

use Doctrine\ORM\Query;

/**
 * ResourceRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ResourceRepository extends \Doctrine\ORM\EntityRepository
{
    public function getAllResourcesByUserId($id){
        return $this->getEntityManager()
            ->createQuery('SELECT r.id, r.mongodbId, r.resourceName, ur1.apiKey
                                FROM AppBundle:Resource r
                                INNER JOIN AppBundle:UsersResources as ur1
                                WITH r.id = ur1.resource
                                WHERE r.id IN(
                                                SELECT IDENTITY(ur.resource)
                                                FROM AppBundle:UsersResources ur
                                                WHERE ur.user = :id AND ur.isCreator = 1)
                                 AND ur1.user = :id'
            )
            ->setParameter('id', $id)
            ->getResult();
    }

    public function getAllSharedResourcesByUserId($id){
        return $this->getEntityManager()
            ->createQuery('SELECT r.id, r.mongodbId, r.resourceName, ur1.apiKey
                                FROM AppBundle:Resource r
                                INNER JOIN AppBundle:UsersResources as ur1
                                WITH r.id = ur1.resource
                                WHERE r.id IN(
                                                SELECT IDENTITY(ur.resource)
                                                FROM AppBundle:UsersResources ur
                                                WHERE ur.user = :id AND ur.isCreator = 0)
                                 AND ur1.user = :id'
            )
            ->setParameter('id', $id)
            ->getResult();
    }

    public function getResourceMongoIdByResourceId($id){
        return $this->getEntityManager()
            ->createQuery('SELECT r.mongodbId FROM AppBundle:Resource as r WHERE r.id = :id')
            ->setParameter('id', $id)
            ->getResult()[0]['mongodbId'];
    }

    public function getResourceByUserIdAndResourceId($userId, $resourceId){

        return $this->getEntityManager()
            ->createQuery('SELECT IDENTITY(ur.resource) as resource_id, ur.apiKey
                                FROM AppBundle:UsersResources as ur 
                                WHERE ur.user = :userId AND ur.resource = :resourceId')
            ->setParameters(['userId' => $userId, 'resourceId' => $resourceId])
            ->getResult();
    }

    public function checkIfUserIsCreatorOfResourceByUserIdAndResourceId($userId, $resourceId){

        return $this->getEntityManager()
            ->createQuery('SELECT ur.isCreator
                                FROM AppBundle:UsersResources as ur 
                                WHERE ur.user = :userId AND ur.resource = :resourceId AND ur.isCreator = 1')
            ->setParameters(['userId' => $userId, 'resourceId' => $resourceId])
            ->getResult();
    }

    public function checkIfResourceNameExistsByResourceNameAndUserId($resourceName, $userId){
        return $this->getEntityManager()
            ->createQuery('SELECT r.resourceName
                                FROM AppBundle:Resource as r
                                INNER JOIN AppBundle:UsersResources as ur
                                WITH r.id = ur.resource
                                WHERE r.resourceName = :resourceName AND ur.user = :userId')
            ->setParameters(['resourceName' => $resourceName, 'userId' => $userId])
            ->getResult();
    }

    public function getResourceAccessLevelIdAndNameByUserIdAndResourceId($userId, $resourceId){
        return $this->getEntityManager()
            ->createQuery('SELECT IDENTITY(ur.access_level), al.name 
                                FROM AppBundle:UsersResources as ur 
                                inner join AppBundle:AccessLevel as al 
                                with ur.access_level = al.id 
                                WHERE ur.user = :user AND ur.resource = :resource')
            ->setParameters(['user' => $userId, 'resource' => $resourceId])
            ->getResult();
    }

    public function getResourceDetails($userId, $resourceId){
        return $this->getEntityManager()
            ->createQuery('SELECT r.id, r.mongodbId, r.resourceName, ur.apiKey, IDENTITY(ur.user)
                                FROM AppBundle:Resource r
                                INNER JOIN AppBundle:UsersResources as ur
                                WITH r.id = ur.resource                               
                                WHERE ur.user = :userId AND ur.resource = :resourceId')
            ->setParameters(['userId' => $userId, 'resourceId' => $resourceId])
            ->getResult();
    }

    public function deleteResourceByResourceId($resourceId){
        return $this->getEntityManager()
            ->createQuery('DELETE 
                                FROM AppBundle:Resource AS r
                                WHERE r.id = :resourceId')
            ->setParameter('resourceId', $resourceId)
            ->getResult();
    }

    public function searchForResources($searchValue, $userId){
        return $this->getEntityManager()
            ->createQuery("SELECT r.id, r.mongodbId, r.resourceName, ur.apiKey, ur.isCreator
                                FROM AppBundle:Resource AS r
                                INNER JOIN AppBundle:UsersResources AS ur
                                WITH r.id = ur.resource
                                WHERE ur.user = :userId AND (r.id LIKE :searchValue OR r.resourceName LIKE :searchValue OR r.mongodbId LIKE :searchValue)")
            ->setParameters(['userId' => $userId, 'searchValue' => '%' . $searchValue . '%'])
            ->getResult();
    }

    public function getNumberOfResourcesOfUserByUserId($userId){
        return $this->getEntityManager()
            ->createQuery("SELECT COUNT(ur.resource) as numberOfResources
                                FROM AppBundle:UsersResources as ur 
                                WHERE ur.user = :userId and ur.isCreator = 1")
            ->setParameter('userId', $userId)
            ->getResult();
    }

    public function getResourceNameById($resourceId){
        return $this->getEntityManager()
            ->createQuery("SELECT r.resourceName
                                FROM AppBundle:Resource as r
                                WHERE r.id = :resourceId")
            ->setParameter('resourceId', $resourceId)
            ->getResult();
    }
}
